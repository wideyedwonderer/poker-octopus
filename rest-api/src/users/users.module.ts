import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TicketsService } from './../tickets/tickets.service';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  imports: [TypeOrmModule.forFeature([])],
  controllers: [UsersController],
  providers: [UsersService, TicketsService],
})
export class UsersModule {}
