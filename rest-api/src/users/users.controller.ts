import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsersUpdateDTO } from './users-register-dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  public constructor(private readonly userService: UsersService) {}

  @Get()
  public async getAllUsers(): Promise<string> {

    return JSON.stringify(await this.userService.findAllUsers());
  }

  @Get(':email')
  public async getUserByEmail(@Param('email') email: string): Promise<string> {

    return JSON.stringify(await this.userService.findUser(email));
  }

  @Post()
  public async createNewAcc(@Body() user: UsersUpdateDTO): Promise<string> {
    const { firstName, lastName, email, password } = user;

    return this.userService.addNewUser(firstName, lastName, email, password);
  }
}
