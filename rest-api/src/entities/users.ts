import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Roles } from './roles';
import { Tickets } from './tickets';

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  public userId: string;

  @Column({ type: 'nvarchar', length: 50, nullable: false })
  public firstName: string;

  @Column({ type: 'nvarchar', length: 50 })
  public lastName: string;

  @Column({ type: 'nvarchar', length: 100, nullable: false })
  public email: string;

  @Column({ type: 'nvarchar', length: 300, nullable: false })
  public password: string;

  @Column({ type: 'tinyint', default: false })
  public banned: boolean;

  @Column({ type: 'date' })
  public lastLogin: string;

  @ManyToOne(() => Roles, (roles) => roles.users, { eager: true })
  @JoinColumn({ name: 'role' })
  public role: Roles;

  @OneToMany(() => Tickets, (tickets) => tickets.receiver)
  public receivedTickets: Tickets[];

  @OneToMany(() => Tickets, (tickets) => tickets.creator)
  public createdTickets: Users[];
}
