import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from 'joi';

export interface IEnvConfig {
  [key: string]: string;
}
@Injectable()
export class ConfigService {
  private readonly envConfig: IEnvConfig;

  public constructor(filePath: string = null) {
    let config: IEnvConfig;
    if (filePath) {
      config = dotenv.parse(fs.readFileSync(filePath));
    } else {
      config = dotenv.config().parsed;
    }
    this.envConfig = this.validateConfig(config);
  }

  private validateConfig(config: IEnvConfig): IEnvConfig {
    const schema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision'])
        .default('development'),
      TYPEORM_PASSWORD: Joi.string().default('root'),
      TYPEORM_HOST: Joi.string().default('localhost'),
      TYPEORM_USERNAME: Joi.string().default('root'),
      TYPEORM_DATABASE: Joi.string().default('poker-octopus'),
      // tslint:disable-next-line:no-magic-numbers
      TYPEORM_PORT: Joi.number().default(3306),
      TYPEORM_SYNCHRONIZE: Joi.string().default('false'),
      TYPEORM_LOGGING: Joi.string().default('true'),
      TYPEORM_ENTITIES: Joi.string().default('src/entities/**.ts'),
      TYPEORM_TYPE: Joi.string().default('mysql'),
      TYPEORM_CONNECTION: Joi.string().default('mysql'),
      TYPEORM_MIGRATIONS_DIR: Joi.string().default('src'),
      TYPEORM_MIGRATIONS: Joi.string().default('migration/*.ts'),
      JWT_SECRET: Joi.string().required(),
      // tslint:disable-next-line:no-magic-numbers
      JWT_EXPIRE: Joi.number().default(3600 * 24 * 7),
      // tslint:disable-next-line:no-magic-numbers
      PORT: Joi.number().default(3000),
    });
    const { error, value } = Joi.validate(config, schema);

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return value;
  }
  public get nodeEnv(): string {
    return this.envConfig.NODE_ENV;
  }
  public get dbPassword(): string {
    return this.envConfig.TYPEORM_PASSWORD;
  }
  public get dbHost(): string {
    return this.envConfig.TYPEORM_HOST;
  }
  public get dbName(): string {
    return this.envConfig.TYPEORM_DATABASE;
  }
  public get dbPort(): number {
    return +this.envConfig.TYPEORM_PORT;
  }
  public get synchronize(): string {
    return this.envConfig.TYPEORM_SYNCHRONIZE;
  }
  public get logging(): string {
    return this.envConfig.TYPEORM_LOGGING;
  }
  public get entities(): string {
    return this.envConfig.TYPEORM_ENTITIES;
  }
  public get dbType(): string {
    return this.envConfig.TYPEORM_TYPE;
  }
  public get connection(): string {
    return this.envConfig.TYPEORM_CONNECTION;
  }
  public get jwtSecret(): string {
    return this.envConfig.JWT_SECRET;
  }

  public get jwtExpireTime(): number {
    return +this.envConfig.JWT_EXPIRE;
  }
  public get port(): number {
    return +this.envConfig.PORT;
  }
}
